Vue.component("coupon", {
  template: `<input placeholder="Enter coupon" @blur="onCouponAppliedChild">`,
  methods: {
    onCouponAppliedChild() {
      this.$emit("coupon-applied");
    }
  }
});

new Vue({
  el: "#app",
  data: {
    couponApplied: false
  },
  methods: {
    onCouponApplied() {
      this.couponApplied = true;
    }
  }
});
