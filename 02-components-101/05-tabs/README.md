# Parent Child

**Didn't understand properly**

Ref: https://youtu.be/-95jgDDZq3Y

Have a parent (tabs) co-ordinating child (tab). Understood binding, props, and methods.

However, I don't know the reason behind in `tabs` section:

```
 <div class="tabs-details">
        <slot></slot>
      </div>
```

Since we loop through tabs and display each tab, I would expect that the html markup for each tab is displayed and only the tab body (`h1` content) is not displayed. But it is not true. None of the markups and tab are displayed. Bit confusing.
