var app = new Vue({
  el: "#app",
  data: {
    newBook: "",
    books: [
      "7 habits",
      "Zen and motorcycle maintenance",
      "How Will You Measure Your Life"
    ]
  },
  methods: {
    addBook() {
      this.books.push(this.newBook);
      this.newBook = "";
    }
  }
});
