import Signin from "./components/Signin.vue";
import Signup from "./components/Signup.vue";

export default [
  {
    path: "/",
    name: "home",
    component: Signin
  },
  {
    path: "/signin",
    name: "signin",
    component: Signin
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup
  }
];
