import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";

import routes from "./routes.js";
import store from "./store";

Vue.config.productionTip = false;

Vue.use(VueRouter);

// create the router instance here.
const router = new VueRouter({
  mode: "history",
  routes
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
