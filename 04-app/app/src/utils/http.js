import axios from "axios";

// ref: https://github.com/axios/axios/issues/1383

const axiosInstance = axios.create({
  // in development use base_url from .env.development file
  // else use /
  baseURL: process.env.VUE_APP_API_BASE_URL
});

export default axiosInstance;
