import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    token: "",
    categories: ""
  },
  getters: {
    token: state => state.token
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    }
  }
});

export default store;
