# Fake Server

It uses `json-server` for json api. You can install it using `npm install -g json-server`.

Create a file `db.json` with some data:

```
{
  "user":[
    {id:1,email:"j1@gmail.com", password:"123456"}
  ]
}
```

Bring up the server with `json-server --watch db.json`

For further reference, refer `https://github.com/typicode/json-server`
