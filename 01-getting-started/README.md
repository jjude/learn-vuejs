# Tutorials

https://scrimba.com/playlist/pXKqta

https://www.youtube.com/watch?v=Y05uRiksXXI&list=PL3VM-unCzF8iRyPotjFsgy7EfuCITvr_3 - Laracast
https://www.youtube.com/playlist?list=PLwAKR305CRO_1yAao-8aZiQnBqJeyng4O - vue fundamentals

https://www.codementor.io/javascript/tutorial/getting-started-with-vuejs

## What are we building

## Steps

### hello world

- copy code from https://gist.githubusercontent.com/chrisvfritz/7f8d7d63000b48493c336e48b3db3e52/raw/ed60c4e5d5c6fec48b0921edaed0cb60be30e87c/index.html

- open html and you have your first vue app

### conditionally displaying message

<span v-if="seen">message</span>
