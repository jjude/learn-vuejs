# hello-world

- Install node @10
- Install vue cli `vue create hello-world`. Press enter key to select the default preset. It will install everyting needed.
- Start dev server by going into the directory and running `npm run serve`

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
